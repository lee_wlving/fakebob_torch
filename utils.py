import numpy as np
import os
from speechbrain.dataio.dataio import read_audio
import subprocess
import shlex

class gmm_ubm_kaldiHelper:
    def __init__(self, pre_model_dir="pre-models", audio_dir=None, mfcc_dir=None, log_dir=None, score_dir=None):
        self.pre_model_dir = os.path.abspath(pre_model_dir)
        self.conf_dir = os.path.join(self.pre_model_dir, "conf")
        audio_dir = audio_dir if audio_dir else "audio"
        self.audio_dir = os.path.abspath(audio_dir)

        mfcc_dir = mfcc_dir if mfcc_dir else "mfcc"
        self.mfcc_dir = os.path.abspath(mfcc_dir)

        log_dir = log_dir if log_dir else "log"
        self.log_dir = os.path.abspath(log_dir)

        score_dir = score_dir if score_dir else "score"
        self.score_dir = os.path.abspath(score_dir)

        ''' deal with the protential permission issue
        '''
        all_files = (self.get_all_files(self.pre_model_dir + "/utils") +
                     self.get_all_files(self.pre_model_dir + "/steps") +
                     self.get_all_files(self.pre_model_dir + "/sid"))

        for file in all_files:
            change_permission_command = "chmod 777 " + file
            args = shlex.split(change_permission_command)
            p = subprocess.Popen(args, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
            p.wait()

    def get_all_files(self, root_dir):

        files = []
        for name in os.listdir(root_dir):
            path = os.path.join(root_dir, name)
            if os.path.isfile(path):
                files.append(path)
            else:
                files_sub = self.get_all_files(path)
                files += files_sub

        return files
